# Raj's Restaurant App

Restaurant App made entirely in Flask

###Technologies Used
- flask
- postgresql

#### Frontend libraries
- Sass
- JS components

## Contents/TODO
1. Generaal setup 
2. Backend: Database connections
3. Frontend: CSS outline
4. User creation and addition 
5. Admin backend for adding food items e.t.c
6. Comming soon
.
├── app
│   ├── assets
│   │   └── scss
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── static
│   │   ├── css
│   │   └── gulpfile.js
│   ├── templates
│   │   ├── contact.html
│   │   ├── index.html
│   │   └── order.html
│   ├── views.py
│   └── views.pyc
├── config.py
├── guni.py
├── README.md
├── requirements.txt
└── werk.py

6 directories, 13 files
