import os
basedir=os.path.abspath(os.path.dirname(__file__))

MAINTENANCE=False
ENV='development'
DEBUG=True
TEMPLATES_AUTO_RELOAD=True

SQLALCHEMY_DATABASE_URI='sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO=os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS=False

WTF_CSRF_ENABLED=True
SECRET_KEY='secret'
WHOOSH_BASE=os.path.join(basedir, 'app.db')
