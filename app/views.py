from app import Raj, admin, db
from flask_admin.contrib.sqla import ModelView
from flask import render_template, url_for

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(80), unique=True, nullable=False)
	email = db.Column(db.String(120), unique=True, nullable=False)	
	def __repr__(self):
		return '<User %r>' % self.username

@Raj.route("/", methods=["GET"])
def index():
	return render_template('index.html', title="Raj's Restaurant")

@Raj.route("/order", methods=["GET","POST"])
def order():
	return render_template("order.html", title="Order form")


@Raj.route("/contact", methods=["GET","POST"])
def contact():
	return render_template("contact.html", title="Contact form")


admin.add_view(ModelView(User, db.session))
