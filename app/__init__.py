from flask import Flask
from flask_admin import Admin
from flask_scss import Scss
from flask_sqlalchemy import SQLAlchemy

Raj = Flask(__name__)
Raj.config.from_object('config')
Scss(Raj)
db = SQLAlchemy(Raj)
admin = Admin(Raj, name="Raj's Restaurant.")

from app import views
